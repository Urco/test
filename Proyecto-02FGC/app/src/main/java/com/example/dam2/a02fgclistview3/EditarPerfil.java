package com.example.dam2.a02fgclistview3;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by urco1 on 22/11/2017.
 */

public class EditarPerfil extends AppCompatActivity {
    private AppBares bd;
    private EditText nombre_perfil;
    private EditText login_perfil;
    private EditText localidad_perfil;

    private Button boton;
    private Button boton_cerrar;
     Usuario user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bd = new AppBares(getApplicationContext());
        Intent intent1 = getIntent();
        user=(Usuario)intent1.getExtras().getSerializable(MainActivity.USUARIO);
        //Log.d("userEdit",usr.getLogin());
        setContentView(R.layout.activity_edit_datos);

        //Los campos para poder actualizar
        nombre_perfil = (EditText) findViewById(R.id.edit_nombre_perfil);
        login_perfil = (EditText) findViewById(R.id.edit_login_perfil);
        localidad_perfil = (EditText) findViewById(R.id.edit_localidad_perfil);

        boton = (Button) findViewById(R.id.edit_boton_actualizar);
        boton_cerrar = (Button) findViewById(R.id.edit_boton_cerrar_actualizar);
        //Lo datos que contiene actualmente


        gestionarEventos();



    }//fin cnCreate


    void gestionarEventos(){
        verDatos();
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                actualizar();
            }
        });

        boton_cerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               enviarDatosUsuario();

            }
        });
    }


    void actualizar(){

        String query;
        SQLiteDatabase sqlLiteDB = bd.getWritableDatabase();
        query="Update usuario Set nombre=?, login=?, localidad=? where Id=?";

        SQLiteStatement statement = sqlLiteDB.compileStatement(query);
        statement.bindString(1, nombre_perfil.getText().toString());
        statement.bindString(2, login_perfil.getText().toString());
        statement.bindString(3, localidad_perfil.getText().toString());
        statement.bindDouble(4, user.getId());
        statement.executeUpdateDelete();

        user.setLogin(login_perfil.getText().toString());

        Toast.makeText(getApplicationContext(),"Datos actualizados", Toast.LENGTH_LONG).show();
    }
    void verDatos(){

        String[] arg = new String[]{Integer.toString(user.getId())};

        SQLiteDatabase db = bd.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select nombre, login, localidad from usuario where  Id=?",arg);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        int index1=cursor.getColumnIndex("nombre");
                        int index2=cursor.getColumnIndex("login");
                        int index3=cursor.getColumnIndex("localidad");

                        String nombre = cursor.getString(index1);
                        String login = cursor.getString(index2);
                        String localidad = cursor.getString(index3);

                        nombre_perfil.setText(nombre);
                        login_perfil.setText(login);
                        localidad_perfil.setText(localidad);

                    } while (cursor.moveToNext());

                }

             }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent1 = new Intent(this, Busqueda_Bares.class);
                intent1.putExtra(MainActivity.USUARIO,this.user);
                startActivity(intent1);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    void enviarDatosUsuario(){
        Intent datosDeVuelta = new Intent();
        datosDeVuelta.putExtra("Usuario",this.user);
        setResult(RESULT_OK,datosDeVuelta);
        finish();
    }

}//fin editarPerfil
