package com.example.dam2.a02fgclistview3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 * Created by urco1 on 07/11/2017.
 */


public class NuevoUsuario extends AppCompatActivity {
    private AppBares appv;
    UsuarioDAOSQLite usrDAO;
    private boolean validacion = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_usuario);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.usrDAO = new UsuarioDAOSQLite(this);
        xestionarEventos();


    }//fin onCreate

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean resultado = false;
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                resultado = super.onOptionsItemSelected(item);

        }
        return resultado;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    void xestionarEventos() {

        final EditText nombre = (EditText) findViewById(R.id.id_nombre);
        listenerCamposVacios(nombre);

        final EditText login2 = (EditText) findViewById(R.id.id_login);
        listenerCamposVacios(login2);

        final EditText contrasena = (EditText) findViewById(R.id.id_password);
        listenerCamposVacios(contrasena);

        listenerCamposVacios2(contrasena);

        Button btnNuevoUsuario = (Button) findViewById(R.id.btnregistrarusuario);
        btnNuevoUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                validacionUsuario(login2, contrasena);
            }

        });
    }//fin XestionarEventos

    private void validacionUsuario(EditText login2, EditText contrasena) {
        boolean validacion = true;

        if (!longitudLogin(login2)) {
            validacion = false;
        }

        if (!coincidenciaContrasena()) {
            validacion = false;

        }

        if (!camposVacios(login2)){
            validacion = false;
        }
        if (!camposVacios(contrasena)){
            validacion = false;
        }

        if (validacion){
            crearUsuario();

        }else{

            mensajeDialogo();
        }
    }

    ///metodo listenerNuevoUsuario
    private void listenerCamposVacios(final EditText param_editText){

        param_editText.addTextChangedListener(new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() <= 0) {
                param_editText.setError("El campo no puede estar vacio");
            }
        }
    });
}//fin metodolistener

    private void listenerCamposVacios2(final EditText password){

        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() >= 9) {
                    password.setError("La contraseña no puede ser de mas de 8 caracteres");

                }

            }
        });
    }//fin metodolistener

    ///////////////metodos comprobacion/////////////////////////

    ////comprobar Longitud Login
    private boolean longitudLogin(EditText login) {
        //String activity_login = ((EditText) findViewById(R.id.editText4)).getText().toString();
        if (login.length()<3) {
            validacion = false;
            mensajeDialogo2(":( tienes mal el activity_login");
        }
        return validacion;
    }

    ///comprobar caracteres y digitos de longitud 8
    private boolean coincidenciaContrasena() {
        validacion = true;
        final String password = ((EditText) findViewById(R.id.id_password)).getText().toString();
        Pattern patron = Pattern.compile("[\\d|\\w]{8}");
        Matcher m = patron.matcher(password);
        boolean coincidencia = m.matches();

        if (!coincidencia) {
            validacion = false;
            mensajeDialogo2(":( La contraseña esta mal");
        }
        return validacion;
    }//fin coincidencia

    ////comprobarCamposVacios
    private boolean camposVacios(EditText vacios) {
        validacion=true;

        if (vacios.getText().length()==0) {
            validacion = false;
            vacios.setError("No puede haber campos vacios");
        }

        return validacion;
    }
    ////////////////creacion usuario////////////////////////

    //metodo creacion usuarios//
    void crearUsuario() {

        String nombre = ((EditText) findViewById(R.id.id_nombre)).getText().toString();
        String login = ((EditText) findViewById(R.id.id_login)).getText().toString();
        String password = ((EditText) findViewById(R.id.id_password)).getText().toString();
        boolean terraza = ((CheckBox)findViewById(R.id.checkbox1)).isChecked();
        String localidad = ((Spinner) findViewById(R.id.spin_localidades)).getSelectedItem().toString();

        RadioGroup radio_group = (RadioGroup) findViewById(R.id.radio_group1);
        String bar = ((RadioButton) this.findViewById(radio_group.getCheckedRadioButtonId())).getText().toString();




        /*Creación el objeto usuario. Dado que id es autoincrementable en la base de datos
        el valor del campo id no será procesado en el método de inserción de usuario.
        Por lo tanto, se le pasará 0, o cualquier otro valor.*/

        Usuario usr = new Usuario(0,nombre, login, password, terraza, bar, localidad);
        boolean insercion = this.usrDAO.insertarUsuario(usr);
        //Notificación de la inserción.
        if (insercion)
            Toast.makeText(getApplicationContext(), R.string.ts_nuevo_usuario_registrado, Toast.LENGTH_LONG).show();
        else
            Toast.makeText(getApplicationContext(), R.string.ts_error_registro_usuario, Toast.LENGTH_LONG).show();
        //Ir a la ventana de inicio de sesión y finalizar la Activity.
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }


    /////////////mensaje Dialogo///////////////////////////////////////

    public void mensajeDialogo() {
        MensajeDialogo d = new MensajeDialogo();
        d.setCancelable(false);
        FragmentManager fm = this.getSupportFragmentManager();

        Bundle args = new Bundle();
        args.putString("error", ";) Up tienes que cambiar unas cosas\n" +
                "-Asegurate de que los campos Login y Password no esta vacios\n" +
                "-Asegurate de que el password no es mayor de 8 caracteres \n" +
                "-Asegurate de que el login tiene minimo 3 caracteres");
        d.setArguments(args);
        d.show(fm, "errorLogin");
    }
    public void mensajeDialogo2(String mensaje) {
        MensajeDialogo d = new MensajeDialogo();
        d.setCancelable(false);
        FragmentManager fm = this.getSupportFragmentManager();

        Bundle args = new Bundle();
        args.putString("error", mensaje);
        d.setArguments(args);
        d.show(fm, "errorLogin");
    }
}//fin class

