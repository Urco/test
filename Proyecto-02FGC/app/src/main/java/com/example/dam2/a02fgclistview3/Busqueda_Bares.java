package com.example.dam2.a02fgclistview3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by urco1 on 20/11/2017.
 */

public class Busqueda_Bares extends AppCompatActivity{

    private EditText nombre_bar;
    private Spinner abierto_cerrado;
    private CheckBox gluten;
    private Button botonBusqueda;
    private Usuario usr;
    private static final int COD_PETICION=6;

//consulta y parametro
    public final static String CONSULTA="consulta";
    public final static String PARAMETRO="parametro";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda_datos);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent1 = getIntent();
        usr=(Usuario)intent1.getExtras().getSerializable(MainActivity.USUARIO);

        Log.d("DEPURACION","UsuarioId " + usr.getId());
        String nuevoTitulo=getResources().getText(R.string.app_busqueda_bares)
                +": "+usr.getLogin();

        setTitle(nuevoTitulo);
        gestionarEventos();

    }//fin onCreate



    void gestionarEventos(){
        this.nombre_bar= (EditText) findViewById(R.id.busqueda_nombre);
        this.abierto_cerrado=(Spinner)findViewById(R.id.busqueda_abierto_cerrado);
        this.gluten=(CheckBox) findViewById(R.id.busqueda_checkbox1);
        this.botonBusqueda=(Button) findViewById(R.id.busqueda_boton_busqueda);

        listenerCamposVacios(nombre_bar);
        //boton de busqueda
        this.botonBusqueda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nombre_bar.length()<=0){
                    Toast.makeText(getApplicationContext(),"El campo nombre no puede estar vacio",Toast.LENGTH_LONG);

                }else{
                    buscar();
                }

            }
        });


    }

    //buscar y mandar consulta
    void buscar(){
        String  query;
        //creamos este array para evitar la inyeccion sql
        //y el resultado de este parametro lo enviamos con intent
        String [] param = new  String[2];



        //le decimos al spiner que obtenga el valor selecionado
        if (abierto_cerrado.getSelectedItem().toString().equals("Abierto")){

             param[0] = "1";
             param[1] = nombre_bar.getText().toString();
        }else{
             param[0] = "0";
            param[1] = nombre_bar.getText().toString();
        }

        //Esta es la consulta que enviamos a Resultado_Busqueda_Bares
        query="Select IdBar as _id, NombreBar, Direccion," +
                " Telefono, AbiertoCerrado, Gluten from Bares where AbiertoCerrado=? or NombreBar=?";

        Log.d("Depu",param[0]);
        Intent intent2 = new Intent(this,Resultado_Busqueda_Bares.class);
        intent2.putExtra(this.CONSULTA,query);
        intent2.putExtra(this.PARAMETRO,param);
        intent2.putExtra(MainActivity.USUARIO,usr);
        startActivity(intent2);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_busqueda, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.editar_perfil:
                //pasamos el usuario al otro activity editarPerfil
                Intent intent1 = new Intent(this, EditarPerfil.class);
                intent1.putExtra(MainActivity.USUARIO,this.usr);
                //startActivity(intent1);
                //En este starActivity le pasamos el numero que despues nos devolvera
                startActivityForResult(intent1,COD_PETICION);
                //finish();
                return true;
            case R.id.logout:
                //cerramos esta activity y pasamos a Busqueda_Bares
                Intent intent2 = new Intent(this, MainActivity.class);
                startActivity(intent2);
                finish();
                return  true;

            case R.id.datos:
                //cerramos esta activity y pasamos a Busqueda_Bares
                Intent intent3 = new Intent(this, DatosUsuarioLogeado.class);
                intent3.putExtra(MainActivity.USUARIO,this.usr);
                startActivity(intent3);
                finish();
                return  true;

            case R.id.maps:
                Intent intent4 = new Intent(this,MapsActivity.class);
                intent4.putExtra(MainActivity.USUARIO,this.usr);
                startActivity(intent4);
                finish();
                return true;
            case R.id.camara_fotos:
                Intent intent5 = new Intent(this,Camara.class);
                intent5.putExtra(MainActivity.USUARIO,this.usr);
                startActivity(intent5);
                finish();
                return true;

            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void listenerCamposVacios(final EditText param_editText){

        param_editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() <= 0) {
                    param_editText.setError("Esta campo no puede estar vacio");
                }
            }
        });
    }//fin metodolistener


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
       if (requestCode==COD_PETICION){
           if (resultCode==RESULT_OK) {
                   usr = (Usuario) data.getExtras().getSerializable("Usuario");
                   Toast.makeText(this, "Login cambiado " + usr.getLogin(), Toast.LENGTH_SHORT).show();
                   setTitle(usr.getLogin());
           }
       }
    }
}//fin BusquedaBares
