package com.example.dam2.a02fgclistview3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by dam207 on 31/10/2017.
 */


public class DatosUsuarioLogeado extends AppCompatActivity {
    private AppBares bd;
    private Usuario user;
    private TextView nombre_datos;
    private TextView login_datos;
    private TextView password_datos;
    private TextView terraza_datos;
    private TextView bar_datos;
    private TextView localidad_datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_usuario);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bd = new AppBares(getApplicationContext());
        Intent intent1 = getIntent();
        user=(Usuario)intent1.getExtras().getSerializable(MainActivity.USUARIO);


        nombre_datos = (TextView) findViewById(R.id.nombre_apellido);
        login_datos = (TextView) findViewById(R.id.id_login);
        password_datos = (TextView) findViewById(R.id.id_password);
        terraza_datos = (TextView) findViewById(R.id.id_terraza);
        bar_datos = (TextView) findViewById(R.id.id_bar);
        localidad_datos = (TextView) findViewById(R.id.id_localidad);

        verDatos();

    }

    void verDatos(){

        String[] arg = new String[]{Integer.toString(user.getId())};

        SQLiteDatabase db = bd.getReadableDatabase();
        Cursor cursor = db.rawQuery("Select nombre, login, password, terraza, bar, localidad from usuario where  Id=?",arg);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    int index1=cursor.getColumnIndex("nombre");
                    int index2=cursor.getColumnIndex("login");
                    int index3=cursor.getColumnIndex("password");
                    int index4=cursor.getColumnIndex("terraza");
                    int index5=cursor.getColumnIndex("bar");
                    int index6=cursor.getColumnIndex("localidad");

                    String nombre = cursor.getString(index1);
                    String login = cursor.getString(index2);
                    String password = cursor.getString(index3);
                    String terraza = cursor.getString(index4);
                    String bar = cursor.getString(index5);
                    String localidad = cursor.getString(index6);

                    nombre_datos.setText(nombre);
                    login_datos.setText(login);
                    password_datos.setText(password);
                    terraza_datos.setText(terraza);
                    bar_datos.setText(bar);
                    localidad_datos.setText(localidad);

                } while (cursor.moveToNext());

            }

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}//fin class
