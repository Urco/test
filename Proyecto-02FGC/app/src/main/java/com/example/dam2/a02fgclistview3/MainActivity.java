package com.example.dam2.a02fgclistview3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Intent;
import android.support.v4.app.FragmentManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class MainActivity extends AppCompatActivity {
    //Declaration variable
    //private AppBares appv;
    public final static String ID = "id";
    public final static String NOMBRE = "nombre";
    public final static String LOGIN="login";
    public final static String PASSWORD="password";
    public final static String TERRAZA ="terraza";
    public final static String BAR   ="bar";
    public final static String LOCALIDAD="localidad";
    public final static String USUARIO="usuario";


    UsuarioDAOSQLite usrDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        copiarBD();
        this.usrDAO = new UsuarioDAOSQLite(this);
        xestionarEventos();
    }//fin on create

//Esto esta modificado otra vez.

    ///copiamos la base de datos
    private void copiarBD() {
        String bddestino = "/data/data/" + getPackageName() + "/databases/"
                + AppBares.NOME_BD;
        File file = new File(bddestino);
        Log.d("DEPURACIÓN", "Ruta archivo BD: " + bddestino);
        if (file.exists()) {
            Toast.makeText(getApplicationContext(), R.string.ts_no_copiar, Toast.LENGTH_LONG).show();
            return; // XA EXISTE A BASE DE DATOS
        }


        String pathbd = "/data/data/" + getPackageName()
                + "/databases/";
        File filepathdb = new File(pathbd);
        filepathdb.mkdirs();


        InputStream inputstream;
        try {
            inputstream = getAssets().open(AppBares.NOME_BD);
            OutputStream outputstream = new FileOutputStream(bddestino);


            int tamread;
            byte[] buffer = new byte[2048];


            while ((tamread = inputstream.read(buffer)) > 0) {
                outputstream.write(buffer, 0, tamread);
            }


            inputstream.close();
            outputstream.flush();
            outputstream.close();
            Toast.makeText(getApplicationContext(), R.string.ts_copiada, Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }//fin copiarBD


    private void xestionarEventos() {
        Button btnAbrirBD = (Button) findViewById(R.id.button);
        btnAbrirBD.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                iniciarSesion();
            }
        });

        Button btnNuevoUsuario = (Button) findViewById(R.id.button2);
        btnNuevoUsuario.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                nuevoUsuario();
            }
        });


    }//fin xestionarEventos


    void iniciarSesion() {
        String login = ((EditText) findViewById(R.id.editText)).getText().toString();
        String password = ((EditText) findViewById(R.id.editText2)).getText().toString();
        Usuario usr = this.usrDAO.getUsuario(login, password);

        if (usr != null) {

            Log.d("DEPURACIÓN", "Nombre usr: "+ usr.getNombre());
            Toast.makeText(getApplicationContext(), R.string.ts_iniciando_sesion, Toast.LENGTH_LONG).show();

            Intent intent2 = new Intent(this,Busqueda_Bares.class);
            intent2.putExtra(this.USUARIO,usr);
            startActivity(intent2);
            //finish();


        } else {
            //Toast.makeText(getApplicationContext(), "Error de autentificación.", Toast.LENGTH_LONG).show();
            MensajeDialogo d= new MensajeDialogo();
            d.setCancelable(false);
            FragmentManager fm= this.getSupportFragmentManager();

            //Usamos un Bunddle para guardar el error
            Bundle args = new Bundle();
            args.putString("error", "Usuario Incorrecto intentalo otra vez :)");
            d.setArguments(args);
            d.show(fm,"errorLogin");
            //esto es una prueba
        }
    }//fin iniciarSesion

    void nuevoUsuario(){
        Toast.makeText(getApplicationContext(),R.string.ts_usuario_incorrecto , Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, NuevoUsuario.class);
        startActivity(intent);

        // No se finaliza la Activity en este caso.--
    }//fin nuevoUsuario


}//fin main
