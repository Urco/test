package com.example.dam2.a02fgclistview3;

import java.io.Serializable;

/**
 * Created by dam207 on 31/10/2017.
 */


public class Usuario implements Serializable{
    String nombre, login, password,localidad, bar;
    int id;
    boolean terraza;


    public Usuario(int id, String nombre, String login, String password, Boolean terraza, String bar, String localidad) {
        this.nombre = nombre;
        this.login = login;
        this.password = password;
        this.id = id;
        this.localidad= localidad;
        this.terraza = terraza;
        this.bar = bar;
    }

    public String getLogin() {
        return login;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public boolean isTerraza() {
        return terraza;
    }

    public void setTerraza(boolean terraza) {
        this.terraza = terraza;
    }

    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }


}
