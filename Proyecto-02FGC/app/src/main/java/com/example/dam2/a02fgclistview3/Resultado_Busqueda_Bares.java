package com.example.dam2.a02fgclistview3;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

/**
 * Created by dam207 on 21/11/2017.
 */

public class Resultado_Busqueda_Bares extends AppCompatActivity {
    //Declaramos la BD
    AppBares bdBares;
    Usuario usr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //este set es para llevarnos al listView
        setContentView(R.layout.listview_resultado_busqueda_bares);
        //Esto nos permite el boton de regreso
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //con este intent recibimos los datos de Busqueda_Bares
        Intent intent1 = getIntent();
        usr =(Usuario)intent1.getExtras().getSerializable(MainActivity.USUARIO);
        String  consulta= intent1.getExtras().getString(Busqueda_Bares.CONSULTA);
        //Este array param recibe la referencia que le debuelve el intent3
        String[] param = intent1.getExtras().getStringArray(Busqueda_Bares.PARAMETRO);
        Log.d("param", param[0]);
        bdBares=new AppBares(getApplicationContext());

        Log.d("DEPURACION", "Nº filas: " + consulta);
        SQLiteDatabase sqlLiteDB = bdBares.getWritableDatabase();
        Cursor cursor = sqlLiteDB.rawQuery(consulta, param);
        Log.d("DEPURACION", "Nº filas: " + cursor.getCount());
        ListView resultadoBares = (ListView) findViewById(R.id.resultadoListview);


        //Añadimos los datos al Adapter y le indicamos donde dibujar cada dato en la fila del Layout
        String[] desdeEstasColumnas = {"NombreBar", "Direccion", "Telefono", "AbiertoCerrado", "Gluten"};

        int[] aEstasViews = {R.id.resultadoNombreBar, R.id.resultadoDireccion, R.id.resultadoTelefono, R.id.resultadoAbiertoCerrado, R.id.resultadoGluten};

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.activity_resultado_busqueda, cursor, desdeEstasColumnas, aEstasViews, 0);
        resultadoBares.setAdapter(adapter);

    }//fin onCreate

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
}