package com.example.dam2.a02fgclistview3;

/**
 * Created by dam207 on 31/10/2017.
 */


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;


public class MensajeDialogo extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle("No se pudo iniciar la sesión").setIcon(android.R.drawable.ic_dialog_alert)
                //usamos get arguments en vez de un constructor cuando usamos fragments
                .setMessage(getArguments().getString("error"))
                .setPositiveButton("ACEPTAR", new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Código asociado al botón Aceptar. Por ejemplo:
                        //Toast.makeText(getActivity(), "PULSADA OPCION BOA", Toast.LENGTH_LONG).show();
                    }
                });
        return builder.create();
    }
}

