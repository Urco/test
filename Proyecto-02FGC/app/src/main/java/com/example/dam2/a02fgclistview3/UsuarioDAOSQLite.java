package com.example.dam2.a02fgclistview3;
/**
 * Created by dam207 on 31/10/2017.
 */



import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class UsuarioDAOSQLite implements UsuarioDAO {
    private AppBares appv;
    private Context context;

    UsuarioDAOSQLite(Context context){
        this.context=context;
        this.appv = new AppBares(this.context);
    }

    public Usuario getUsuario(String login, String password) {
        Usuario resultado=null;

        SQLiteDatabase sqlLiteDB = appv.getWritableDatabase();
        String[] param = {login, password};
        String consulta = "SELECT * FROM usuario WHERE login=? AND password=?";
        Cursor cursor = sqlLiteDB.rawQuery(consulta, param);
        this.depuracion(consulta, param);
        Log.d("DEPURACIÓN", "Nº filas: " + cursor.getCount());
        if (cursor.moveToFirst()) {
            boolean flag = (cursor.getString(4).equals("0")?true:false);
            Log.d("BOO", "Flag valro "+flag);
            resultado = new Usuario(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),flag, cursor.getString(5), cursor.getString(6));
        }
        return resultado;
    }

    @Override
    public boolean insertarUsuario(Usuario usr) {
        boolean resultado = true;
        SQLiteDatabase sqlLiteDB = appv.getWritableDatabase();
        String sql = "INSERT INTO Usuario (nombre, login, password, terraza, bar, localidad) VALUES (?, ?, ?, ?, ?, ?)";
        SQLiteStatement statement = sqlLiteDB.compileStatement(sql);

        statement.bindString(1, usr.getNombre());
        statement.bindString(2, usr.getLogin());
        statement.bindString(3, usr.getPassword());
        int flag1 = (usr.isTerraza()?0:1);
        statement.bindString(4, Integer.toString(flag1));
        statement.bindString(5, usr.getBar());
        statement.bindString(6, usr.getLocalidad());


        long rowId = statement.executeInsert();

        if (rowId != -1) {
            //Comprobación de la lista de usuarios. El siguiente código tiene como finalidad
            //mostrar en el logcat el usuario que se acaba de insertar.
            String usuarios = "";
            Cursor cursor = sqlLiteDB.rawQuery("select * from Usuario", null);
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    usuarios+= "\n" + cursor.getInt(0) + " " + cursor.getString(1) + " " + cursor.getString(2) + " " + cursor.getString(3)
                            + " " + cursor.getInt(4) + " " + cursor.getString(5)+ " " + cursor.getString(6);
                    cursor.moveToNext();
                }
            }
            Log.d("DEPURACIÓN", "Resultado inserción: 02FGC" + usuarios);
            Log.d("DEPURACIÓN", "Row ID: " + rowId);



        } else {
            resultado = false;
        }
        return resultado;

    }

    void depuracion(String consulta, String[] param) {
        String texto = "Consulta: " + consulta + " Valores: ";
        for (String p : param) {
            texto += p + " ";
        }
        Log.d("DEPURACIÓN", texto);
    }
}

