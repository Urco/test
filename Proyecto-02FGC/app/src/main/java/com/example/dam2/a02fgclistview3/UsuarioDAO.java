package com.example.dam2.a02fgclistview3;

/**
 * Created by dam207 on 31/10/2017.
 */


public interface UsuarioDAO {
    Usuario getUsuario(String login, String password);
    boolean insertarUsuario(Usuario usr);

}

